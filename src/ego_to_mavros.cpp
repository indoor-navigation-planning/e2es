#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/State.h>
#include <cstdio>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <math.h>
#include <mutex>
#include <std_msgs/String.h>
#include <utils/include/all_utils.h>
#include <movement/generalmove.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
//#include <curses.h>
//#include <ncurses.h>

//subscribe local_position/pose
//publish   setpoint/pose
#include <termios.h>

#include "quadrotor_msgs/PositionCommand.h"
#include <mavros_msgs/PositionTarget.h>

static std::mutex mtx_states_RW;

#define PI (3.1415926)

#define DEFAULT_TAKE_OFF_HEIGHT (1.5)

using namespace std;

static enum Mission_STATE {
  IDLE,
  TAKEOFF,
  KEYBOARD_CTR,
  LAND,
} mission_state=IDLE;

static enum MANEUVER{
  KB_NONE,
  KB_TAKEOFF,
  KB_LAND,
  KB_FORWARD,
  KB_BACKWARD,
  KB_LEFT,
  KB_RIGHT,
  KB_UP,
  KB_DOWN,
  KB_TURNLEFT,
  KB_TURNRIGHT,
} kb_state=KB_NONE;

static mavros_msgs::State current_state;
static bool local_pose_received = false;
static SE3 latest_pos;
static geometry_msgs::PoseStamped last_published_pose;
static geometry_msgs::PoseStamped publish_pose;
static double h_speed;
static double v_speed;
static double turn_speed;
static double update_rate=20.0;

mavros_msgs::PositionTarget poscmd;


void state_cb(const mavros_msgs::State::ConstPtr& msg){
  current_state = *msg;
}

void local_odom_cb(const geometry_msgs::PoseStampedConstPtr& msg){
  mtx_states_RW.lock();
  local_pose_received = true;
  latest_pos.translation().x()=msg->pose.position.x;
  latest_pos.translation().y()=msg->pose.position.y;
  latest_pos.translation().z()=msg->pose.position.z;
  latest_pos.so3() = SO3(Quaterniond(msg->pose.orientation.w,msg->pose.orientation.x,
                                     msg->pose.orientation.y,msg->pose.orientation.z));
  mtx_states_RW.unlock();
}

geometry_msgs::PoseStamped update_pose_from_se3(SE3 se3)
{
  geometry_msgs::PoseStamped pose;
  pose.header.frame_id = "world";
  pose.pose.position.x = se3.translation().x();
  pose.pose.position.y = se3.translation().y();
  pose.pose.position.z = se3.translation().z();
  pose.pose.orientation.x = se3.so3().unit_quaternion().x();
  pose.pose.orientation.y = se3.so3().unit_quaternion().y();
  pose.pose.orientation.z = se3.so3().unit_quaternion().z();
  pose.pose.orientation.w = se3.so3().unit_quaternion().w();
  return pose;
}

void position_cmd_cb(const quadrotor_msgs::PositionCommand::ConstPtr& cmd){

    poscmd.header.stamp =  ros::Time::now();
    poscmd.header.frame_id = "world";
    poscmd.header.seq += 1;
    poscmd.coordinate_frame = mavros_msgs::PositionTarget::FRAME_LOCAL_NED;

    poscmd.type_mask = 0;

    poscmd.position = cmd->position;

    poscmd.velocity = cmd->velocity;

    poscmd.acceleration_or_force = cmd->acceleration;

    poscmd.yaw = cmd->yaw;
    poscmd.yaw_rate = cmd->yaw_dot / update_rate;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ego_to_mavros");
  //initscr();

  ros::NodeHandle nh("~");

  nh.getParam("velocity_horizon", h_speed);
  nh.getParam("velocity_vertical", v_speed);
  nh.getParam("angular_velocity_turn", turn_speed);
  cout << "horizon speed: " <<  h_speed << endl;
  cout << "vertical speed: " << v_speed << endl;
  cout << "turning speed: " <<  turn_speed << endl;

  ros::Subscriber state_sub = nh.subscribe<mavros_msgs::State>
      ("/mavros/state", 10, state_cb);
  ros::Subscriber local_odom_sub = nh.subscribe<geometry_msgs::PoseStamped>
      ("/mavros/local_position/pose", 10, local_odom_cb);

  ros::Publisher local_pos_pub = nh.advertise<geometry_msgs::PoseStamped>
      ("/mavros/setpoint_position/local", 10);
  ros::ServiceClient arming_client = nh.serviceClient<mavros_msgs::CommandBool>
      ("/mavros/cmd/arming");
  ros::ServiceClient set_mode_client = nh.serviceClient<mavros_msgs::SetMode>
      ("/mavros/set_mode");

  ros::Publisher local_pos_cmd = nh.advertise<mavros_msgs::PositionTarget>
            ("/mavros/setpoint_raw/local", 100);
  
  // ros::Subscriber pose_sub = nh.subscribe<quadrotor_msgs::PositionCommand>
  //           ("/drone_0_planning/pos_cmd", 100, position_cmd_cb);
  ros::Subscriber pose_sub = nh.subscribe<quadrotor_msgs::PositionCommand>
  ("/planning/ref_traj", 100, position_cmd_cb); ////

  //the setpoint publishing rate MUST be faster than 2Hz
  ros::Rate rate(update_rate);
  //wait for FCU connection
  cout << "Waiting for FCU connection " << endl;
  while(ros::ok() && !current_state.connected){
    ros::spinOnce();
    rate.sleep();
    cout << "Waiting for FCU connection " << endl;
  }
  while(local_pose_received==false)
  {
    ros::spinOnce();
    rate.sleep();
    cout << "Waiting for Local Position" << endl;
  }

  mavros_msgs::SetMode offb_set_mode;
  offb_set_mode.request.custom_mode = "OFFBOARD";
  mavros_msgs::CommandBool arm_cmd;
  arm_cmd.request.value = true;
  cout << "change last_request Arm" << endl;
  ros::Time last_request = ros::Time::now();

  while(ros::ok()){

    if( current_state.mode != "OFFBOARD" && (ros::Time::now() - last_request > ros::Duration(1.0)))
    {
      if( set_mode_client.call(offb_set_mode) &&
          offb_set_mode.response.mode_sent){
        ROS_INFO(">>>>>>>>>>>>>>>>>>>>>>>> Offboard enabled");
      }
      last_request = ros::Time::now();
    } else {
      if( !current_state.armed &&
          (ros::Time::now() - last_request > ros::Duration(1.0))){
        if( arming_client.call(arm_cmd) &&
            arm_cmd.response.success){
          ROS_INFO(">>>>>>>>>>>>>>>>>>> Vehicle armed");
          mission_state = IDLE;

          geometry_msgs::PoseStamped pose;
          pose.pose.position.x = 0;
          pose.pose.position.y = 0;
          pose.pose.position.z = DEFAULT_TAKE_OFF_HEIGHT;

          //send a few setpoints before starting
          for(int i = 100; ros::ok() && i > 0; --i){
              local_pos_pub.publish(pose);
              ros::spinOnce();
              rate.sleep();
          }

          publish_pose.pose = pose.pose;

          //Initialze conversion message
          poscmd.header.stamp =  ros::Time::now();
          poscmd.header.frame_id = "world";
          poscmd.header.seq += 1;
          poscmd.coordinate_frame = mavros_msgs::PositionTarget::FRAME_LOCAL_NED;
          poscmd.type_mask = mavros_msgs::PositionTarget::IGNORE_YAW | mavros_msgs::PositionTarget::IGNORE_YAW_RATE ;

          poscmd.position = pose.pose.position;
        }
        last_request = ros::Time::now();
      }
    }

    local_pos_cmd.publish(poscmd);

    last_published_pose = publish_pose;
    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
